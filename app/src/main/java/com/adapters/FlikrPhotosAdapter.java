package com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.notifier.ScrollNotifier;
import com.medtrailassignment.R;
import com.models.UnsplashData;

import java.util.List;

public class FlikrPhotosAdapter extends RecyclerView.Adapter<FlikrPhotosAdapter.FlickrPhotosHolder> {


    private Context context;
    private ScrollNotifier scrollNotifier;
    private List<UnsplashData.ResultsBean> photosList;

    public FlikrPhotosAdapter(Context context, ScrollNotifier scrollNotifier, List<UnsplashData.ResultsBean> photosList) {
        this.context = context;
        this.scrollNotifier = scrollNotifier;
        this.photosList = photosList;
    }

    @NonNull
    @Override
    public FlikrPhotosAdapter.FlickrPhotosHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.flickr_image_container, parent, false);
        return new FlikrPhotosAdapter.FlickrPhotosHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlikrPhotosAdapter.FlickrPhotosHolder holder, int position) {

        UnsplashData.ResultsBean resultsBean = photosList.get(position);
        Glide.with(context).load(resultsBean.getUrls().getSmall()).into(holder.ivPhoto);
        if (position == photosList.size()-1)
            scrollNotifier.onScrolledToBottom();
    }

    @Override
    public int getItemCount() {
        if (photosList != null) {
            return photosList == null ? 0 : photosList.size();
        }
        return 0;
    }

    public class FlickrPhotosHolder extends RecyclerView.ViewHolder {

        private ImageView ivPhoto;

        public FlickrPhotosHolder(@NonNull View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.ivPhoto);
        }
    }
}
