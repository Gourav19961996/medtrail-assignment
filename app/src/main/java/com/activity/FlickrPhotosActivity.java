package com.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.adapters.FlikrPhotosAdapter;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.notifier.ScrollNotifier;
import com.medtrailassignment.R;
import com.medtrailassignment.databinding.ActivityFlickrphotosBinding;
import com.models.UnsplashData;

import java.util.ArrayList;
import java.util.List;

public class FlickrPhotosActivity extends AppCompatActivity implements View.OnClickListener, ScrollNotifier {

    private static final String TAG = FlickrPhotosActivity.class.getCanonicalName();
    private ActivityFlickrphotosBinding binding;
    //    private FlickrPhotosModel mFlickrPhotosModel;
    private FlikrPhotosAdapter flikrPhotosAdapter;
    private int currentPage, totalPages;
    private List<UnsplashData.ResultsBean> photosList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private boolean isNetworkRequestRunning = false;
    private String searchQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_flickrphotos);

        if (getSupportActionBar() != null) {
            this.getSupportActionBar().hide();
        }

        isNetworkRequestRunning = true;

        //listeners
        binding.ivFlickrSearch.setOnClickListener(this);
        binding.etFlickrSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    binding.ivFlickrSearch.performClick();
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivFlickrSearch:
                searchQuery = binding.etFlickrSearch.getText().toString().trim();
                if (!searchQuery.equals("")) {
                    photosList.clear();
                    requestPhotosData(searchQuery, 1);
                }
                break;
        }
    }

    private void requestPhotosData(String searchQuery, int requestedPage) {
        if(!isNetworkAvailable()){
            Toast.makeText(this, "No Internet Connection, Try Again Later!", Toast.LENGTH_SHORT).show();
            return;
        }
        binding.progressBar.setVisibility(View.VISIBLE);
        currentPage = requestedPage;

        AndroidNetworking.get("https://api.unsplash.com/search/photos/?client_id=WEbOk8fiOa34FhwQ_iseeuDE_kWb28xbHHcEuBkhIxA&per_page=20")
                .addQueryParameter("query", searchQuery)
                .addQueryParameter("page", String.valueOf(requestedPage))
                .build()
                .getAsObject(UnsplashData.class, new ParsedRequestListener<UnsplashData>() {
                    @Override
                    public void onResponse(UnsplashData unsplashData) {
                        isNetworkRequestRunning = false;
                        totalPages = unsplashData.getTotalPages();
                        if (flikrPhotosAdapter == null) {
                            initRecyclerView(unsplashData);
                            binding.progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            photosList.addAll(unsplashData.getResults());
                            binding.rvFlickrPhotos.post(new Runnable() {
                                @Override
                                public void run() {
                                    flikrPhotosAdapter.notifyDataSetChanged();
                                }
                            });
                            binding.progressBar.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getErrorBody());
                    }
                });

    }

    private void initRecyclerView(UnsplashData unsplashData) {
        layoutManager = new GridLayoutManager(FlickrPhotosActivity.this, 2);
        binding.rvFlickrPhotos.setLayoutManager(layoutManager);
        photosList.addAll(unsplashData.getResults());
        flikrPhotosAdapter = new FlikrPhotosAdapter(this, this, photosList);
        binding.rvFlickrPhotos.setAdapter(flikrPhotosAdapter);
    }

    @Override
    public void onScrolledToBottom() {
        if (currentPage < totalPages && !isNetworkRequestRunning) {
            currentPage++;
            Toast.makeText(this, "Page: " + currentPage, Toast.LENGTH_SHORT).show();
            requestPhotosData(searchQuery, currentPage);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}