package com.notifier;

public interface ScrollNotifier {

    public void onScrolledToBottom();
}
