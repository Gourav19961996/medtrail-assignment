package com.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UnsplashData {


    @SerializedName("total")
    private int total;
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("results")
    private List<ResultsBean> results;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        @SerializedName("id")
        private String id;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("promoted_at")
        private String promotedAt;
        @SerializedName("width")
        private int width;
        @SerializedName("height")
        private int height;
        @SerializedName("color")
        private String color;
        @SerializedName("blur_hash")
        private String blurHash;
        @SerializedName("description")
        private String description;
        @SerializedName("alt_description")
        private String altDescription;
        @SerializedName("urls")
        private UrlsBean urls;
        @SerializedName("links")
        private LinksBean links;
        @SerializedName("likes")
        private int likes;
        @SerializedName("liked_by_user")
        private boolean likedByUser;
        @SerializedName("sponsorship")
        private Object sponsorship;
        @SerializedName("user")
        private UserBean user;
        @SerializedName("categories")
        private List<?> categories;
        @SerializedName("current_user_collections")
        private List<?> currentUserCollections;
        @SerializedName("tags")
        private List<TagsBean> tags;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getPromotedAt() {
            return promotedAt;
        }

        public void setPromotedAt(String promotedAt) {
            this.promotedAt = promotedAt;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getBlurHash() {
            return blurHash;
        }

        public void setBlurHash(String blurHash) {
            this.blurHash = blurHash;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAltDescription() {
            return altDescription;
        }

        public void setAltDescription(String altDescription) {
            this.altDescription = altDescription;
        }

        public UrlsBean getUrls() {
            return urls;
        }

        public void setUrls(UrlsBean urls) {
            this.urls = urls;
        }

        public LinksBean getLinks() {
            return links;
        }

        public void setLinks(LinksBean links) {
            this.links = links;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }

        public boolean isLikedByUser() {
            return likedByUser;
        }

        public void setLikedByUser(boolean likedByUser) {
            this.likedByUser = likedByUser;
        }

        public Object getSponsorship() {
            return sponsorship;
        }

        public void setSponsorship(Object sponsorship) {
            this.sponsorship = sponsorship;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public List<?> getCategories() {
            return categories;
        }

        public void setCategories(List<?> categories) {
            this.categories = categories;
        }

        public List<?> getCurrentUserCollections() {
            return currentUserCollections;
        }

        public void setCurrentUserCollections(List<?> currentUserCollections) {
            this.currentUserCollections = currentUserCollections;
        }

        public List<TagsBean> getTags() {
            return tags;
        }

        public void setTags(List<TagsBean> tags) {
            this.tags = tags;
        }

        public static class UrlsBean {
            @SerializedName("raw")
            private String raw;
            @SerializedName("full")
            private String full;
            @SerializedName("regular")
            private String regular;
            @SerializedName("small")
            private String small;
            @SerializedName("thumb")
            private String thumb;

            public String getRaw() {
                return raw;
            }

            public void setRaw(String raw) {
                this.raw = raw;
            }

            public String getFull() {
                return full;
            }

            public void setFull(String full) {
                this.full = full;
            }

            public String getRegular() {
                return regular;
            }

            public void setRegular(String regular) {
                this.regular = regular;
            }

            public String getSmall() {
                return small;
            }

            public void setSmall(String small) {
                this.small = small;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }
        }

        public static class LinksBean {
            @SerializedName("self")
            private String self;
            @SerializedName("html")
            private String html;
            @SerializedName("download")
            private String download;
            @SerializedName("download_location")
            private String downloadLocation;

            public String getSelf() {
                return self;
            }

            public void setSelf(String self) {
                this.self = self;
            }

            public String getHtml() {
                return html;
            }

            public void setHtml(String html) {
                this.html = html;
            }

            public String getDownload() {
                return download;
            }

            public void setDownload(String download) {
                this.download = download;
            }

            public String getDownloadLocation() {
                return downloadLocation;
            }

            public void setDownloadLocation(String downloadLocation) {
                this.downloadLocation = downloadLocation;
            }
        }

        public static class UserBean {
            @SerializedName("id")
            private String id;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("username")
            private String username;
            @SerializedName("name")
            private String name;
            @SerializedName("first_name")
            private String firstName;
            @SerializedName("last_name")
            private String lastName;
            @SerializedName("twitter_username")
            private String twitterUsername;
            @SerializedName("portfolio_url")
            private Object portfolioUrl;
            @SerializedName("bio")
            private Object bio;
            @SerializedName("location")
            private String location;
            @SerializedName("links")
            private LinksBeanX links;
            @SerializedName("profile_image")
            private ProfileImageBean profileImage;
            @SerializedName("instagram_username")
            private String instagramUsername;
            @SerializedName("total_collections")
            private int totalCollections;
            @SerializedName("total_likes")
            private int totalLikes;
            @SerializedName("total_photos")
            private int totalPhotos;
            @SerializedName("accepted_tos")
            private boolean acceptedTos;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            public String getTwitterUsername() {
                return twitterUsername;
            }

            public void setTwitterUsername(String twitterUsername) {
                this.twitterUsername = twitterUsername;
            }

            public Object getPortfolioUrl() {
                return portfolioUrl;
            }

            public void setPortfolioUrl(Object portfolioUrl) {
                this.portfolioUrl = portfolioUrl;
            }

            public Object getBio() {
                return bio;
            }

            public void setBio(Object bio) {
                this.bio = bio;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public LinksBeanX getLinks() {
                return links;
            }

            public void setLinks(LinksBeanX links) {
                this.links = links;
            }

            public ProfileImageBean getProfileImage() {
                return profileImage;
            }

            public void setProfileImage(ProfileImageBean profileImage) {
                this.profileImage = profileImage;
            }

            public String getInstagramUsername() {
                return instagramUsername;
            }

            public void setInstagramUsername(String instagramUsername) {
                this.instagramUsername = instagramUsername;
            }

            public int getTotalCollections() {
                return totalCollections;
            }

            public void setTotalCollections(int totalCollections) {
                this.totalCollections = totalCollections;
            }

            public int getTotalLikes() {
                return totalLikes;
            }

            public void setTotalLikes(int totalLikes) {
                this.totalLikes = totalLikes;
            }

            public int getTotalPhotos() {
                return totalPhotos;
            }

            public void setTotalPhotos(int totalPhotos) {
                this.totalPhotos = totalPhotos;
            }

            public boolean isAcceptedTos() {
                return acceptedTos;
            }

            public void setAcceptedTos(boolean acceptedTos) {
                this.acceptedTos = acceptedTos;
            }

            public static class LinksBeanX {
                @SerializedName("self")
                private String self;
                @SerializedName("html")
                private String html;
                @SerializedName("photos")
                private String photos;
                @SerializedName("likes")
                private String likes;
                @SerializedName("portfolio")
                private String portfolio;
                @SerializedName("following")
                private String following;
                @SerializedName("followers")
                private String followers;

                public String getSelf() {
                    return self;
                }

                public void setSelf(String self) {
                    this.self = self;
                }

                public String getHtml() {
                    return html;
                }

                public void setHtml(String html) {
                    this.html = html;
                }

                public String getPhotos() {
                    return photos;
                }

                public void setPhotos(String photos) {
                    this.photos = photos;
                }

                public String getLikes() {
                    return likes;
                }

                public void setLikes(String likes) {
                    this.likes = likes;
                }

                public String getPortfolio() {
                    return portfolio;
                }

                public void setPortfolio(String portfolio) {
                    this.portfolio = portfolio;
                }

                public String getFollowing() {
                    return following;
                }

                public void setFollowing(String following) {
                    this.following = following;
                }

                public String getFollowers() {
                    return followers;
                }

                public void setFollowers(String followers) {
                    this.followers = followers;
                }
            }

            public static class ProfileImageBean {
                @SerializedName("small")
                private String small;
                @SerializedName("medium")
                private String medium;
                @SerializedName("large")
                private String large;

                public String getSmall() {
                    return small;
                }

                public void setSmall(String small) {
                    this.small = small;
                }

                public String getMedium() {
                    return medium;
                }

                public void setMedium(String medium) {
                    this.medium = medium;
                }

                public String getLarge() {
                    return large;
                }

                public void setLarge(String large) {
                    this.large = large;
                }
            }
        }

        public static class TagsBean {
            @SerializedName("type")
            private String type;
            @SerializedName("title")
            private String title;
            @SerializedName("source")
            private SourceBean source;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public SourceBean getSource() {
                return source;
            }

            public void setSource(SourceBean source) {
                this.source = source;
            }

            public static class SourceBean {
                @SerializedName("ancestry")
                private AncestryBean ancestry;
                @SerializedName("title")
                private String title;
                @SerializedName("subtitle")
                private String subtitle;
                @SerializedName("description")
                private String description;
                @SerializedName("meta_title")
                private String metaTitle;
                @SerializedName("meta_description")
                private String metaDescription;
                @SerializedName("cover_photo")
                private CoverPhotoBean coverPhoto;

                public AncestryBean getAncestry() {
                    return ancestry;
                }

                public void setAncestry(AncestryBean ancestry) {
                    this.ancestry = ancestry;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getSubtitle() {
                    return subtitle;
                }

                public void setSubtitle(String subtitle) {
                    this.subtitle = subtitle;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getMetaTitle() {
                    return metaTitle;
                }

                public void setMetaTitle(String metaTitle) {
                    this.metaTitle = metaTitle;
                }

                public String getMetaDescription() {
                    return metaDescription;
                }

                public void setMetaDescription(String metaDescription) {
                    this.metaDescription = metaDescription;
                }

                public CoverPhotoBean getCoverPhoto() {
                    return coverPhoto;
                }

                public void setCoverPhoto(CoverPhotoBean coverPhoto) {
                    this.coverPhoto = coverPhoto;
                }

                public static class AncestryBean {
                    @SerializedName("type")
                    private TypeBean type;
                    @SerializedName("category")
                    private CategoryBean category;
                    @SerializedName("subcategory")
                    private SubcategoryBean subcategory;

                    public TypeBean getType() {
                        return type;
                    }

                    public void setType(TypeBean type) {
                        this.type = type;
                    }

                    public CategoryBean getCategory() {
                        return category;
                    }

                    public void setCategory(CategoryBean category) {
                        this.category = category;
                    }

                    public SubcategoryBean getSubcategory() {
                        return subcategory;
                    }

                    public void setSubcategory(SubcategoryBean subcategory) {
                        this.subcategory = subcategory;
                    }

                    public static class TypeBean {
                        @SerializedName("slug")
                        private String slug;
                        @SerializedName("pretty_slug")
                        private String prettySlug;

                        public String getSlug() {
                            return slug;
                        }

                        public void setSlug(String slug) {
                            this.slug = slug;
                        }

                        public String getPrettySlug() {
                            return prettySlug;
                        }

                        public void setPrettySlug(String prettySlug) {
                            this.prettySlug = prettySlug;
                        }
                    }

                    public static class CategoryBean {
                        @SerializedName("slug")
                        private String slug;
                        @SerializedName("pretty_slug")
                        private String prettySlug;

                        public String getSlug() {
                            return slug;
                        }

                        public void setSlug(String slug) {
                            this.slug = slug;
                        }

                        public String getPrettySlug() {
                            return prettySlug;
                        }

                        public void setPrettySlug(String prettySlug) {
                            this.prettySlug = prettySlug;
                        }
                    }

                    public static class SubcategoryBean {
                        @SerializedName("slug")
                        private String slug;
                        @SerializedName("pretty_slug")
                        private String prettySlug;

                        public String getSlug() {
                            return slug;
                        }

                        public void setSlug(String slug) {
                            this.slug = slug;
                        }

                        public String getPrettySlug() {
                            return prettySlug;
                        }

                        public void setPrettySlug(String prettySlug) {
                            this.prettySlug = prettySlug;
                        }
                    }
                }

                public static class CoverPhotoBean {
                    @SerializedName("id")
                    private String id;
                    @SerializedName("created_at")
                    private String createdAt;
                    @SerializedName("updated_at")
                    private String updatedAt;
                    @SerializedName("promoted_at")
                    private String promotedAt;
                    @SerializedName("width")
                    private int width;
                    @SerializedName("height")
                    private int height;
                    @SerializedName("color")
                    private String color;
                    @SerializedName("blur_hash")
                    private String blurHash;
                    @SerializedName("description")
                    private String description;
                    @SerializedName("alt_description")
                    private String altDescription;
                    @SerializedName("urls")
                    private UrlsBeanX urls;
                    @SerializedName("links")
                    private LinksBeanXX links;
                    @SerializedName("likes")
                    private int likes;
                    @SerializedName("liked_by_user")
                    private boolean likedByUser;
                    @SerializedName("sponsorship")
                    private Object sponsorship;
                    @SerializedName("user")
                    private UserBeanX user;
                    @SerializedName("categories")
                    private List<?> categories;
                    @SerializedName("current_user_collections")
                    private List<?> currentUserCollections;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public String getCreatedAt() {
                        return createdAt;
                    }

                    public void setCreatedAt(String createdAt) {
                        this.createdAt = createdAt;
                    }

                    public String getUpdatedAt() {
                        return updatedAt;
                    }

                    public void setUpdatedAt(String updatedAt) {
                        this.updatedAt = updatedAt;
                    }

                    public String getPromotedAt() {
                        return promotedAt;
                    }

                    public void setPromotedAt(String promotedAt) {
                        this.promotedAt = promotedAt;
                    }

                    public int getWidth() {
                        return width;
                    }

                    public void setWidth(int width) {
                        this.width = width;
                    }

                    public int getHeight() {
                        return height;
                    }

                    public void setHeight(int height) {
                        this.height = height;
                    }

                    public String getColor() {
                        return color;
                    }

                    public void setColor(String color) {
                        this.color = color;
                    }

                    public String getBlurHash() {
                        return blurHash;
                    }

                    public void setBlurHash(String blurHash) {
                        this.blurHash = blurHash;
                    }

                    public String getDescription() {
                        return description;
                    }

                    public void setDescription(String description) {
                        this.description = description;
                    }

                    public String getAltDescription() {
                        return altDescription;
                    }

                    public void setAltDescription(String altDescription) {
                        this.altDescription = altDescription;
                    }

                    public UrlsBeanX getUrls() {
                        return urls;
                    }

                    public void setUrls(UrlsBeanX urls) {
                        this.urls = urls;
                    }

                    public LinksBeanXX getLinks() {
                        return links;
                    }

                    public void setLinks(LinksBeanXX links) {
                        this.links = links;
                    }

                    public int getLikes() {
                        return likes;
                    }

                    public void setLikes(int likes) {
                        this.likes = likes;
                    }

                    public boolean isLikedByUser() {
                        return likedByUser;
                    }

                    public void setLikedByUser(boolean likedByUser) {
                        this.likedByUser = likedByUser;
                    }

                    public Object getSponsorship() {
                        return sponsorship;
                    }

                    public void setSponsorship(Object sponsorship) {
                        this.sponsorship = sponsorship;
                    }

                    public UserBeanX getUser() {
                        return user;
                    }

                    public void setUser(UserBeanX user) {
                        this.user = user;
                    }

                    public List<?> getCategories() {
                        return categories;
                    }

                    public void setCategories(List<?> categories) {
                        this.categories = categories;
                    }

                    public List<?> getCurrentUserCollections() {
                        return currentUserCollections;
                    }

                    public void setCurrentUserCollections(List<?> currentUserCollections) {
                        this.currentUserCollections = currentUserCollections;
                    }

                    public static class UrlsBeanX {
                        @SerializedName("raw")
                        private String raw;
                        @SerializedName("full")
                        private String full;
                        @SerializedName("regular")
                        private String regular;
                        @SerializedName("small")
                        private String small;
                        @SerializedName("thumb")
                        private String thumb;

                        public String getRaw() {
                            return raw;
                        }

                        public void setRaw(String raw) {
                            this.raw = raw;
                        }

                        public String getFull() {
                            return full;
                        }

                        public void setFull(String full) {
                            this.full = full;
                        }

                        public String getRegular() {
                            return regular;
                        }

                        public void setRegular(String regular) {
                            this.regular = regular;
                        }

                        public String getSmall() {
                            return small;
                        }

                        public void setSmall(String small) {
                            this.small = small;
                        }

                        public String getThumb() {
                            return thumb;
                        }

                        public void setThumb(String thumb) {
                            this.thumb = thumb;
                        }
                    }

                    public static class LinksBeanXX {
                        @SerializedName("self")
                        private String self;
                        @SerializedName("html")
                        private String html;
                        @SerializedName("download")
                        private String download;
                        @SerializedName("download_location")
                        private String downloadLocation;

                        public String getSelf() {
                            return self;
                        }

                        public void setSelf(String self) {
                            this.self = self;
                        }

                        public String getHtml() {
                            return html;
                        }

                        public void setHtml(String html) {
                            this.html = html;
                        }

                        public String getDownload() {
                            return download;
                        }

                        public void setDownload(String download) {
                            this.download = download;
                        }

                        public String getDownloadLocation() {
                            return downloadLocation;
                        }

                        public void setDownloadLocation(String downloadLocation) {
                            this.downloadLocation = downloadLocation;
                        }
                    }

                    public static class UserBeanX {
                        @SerializedName("id")
                        private String id;
                        @SerializedName("updated_at")
                        private String updatedAt;
                        @SerializedName("username")
                        private String username;
                        @SerializedName("name")
                        private String name;
                        @SerializedName("first_name")
                        private String firstName;
                        @SerializedName("last_name")
                        private String lastName;
                        @SerializedName("twitter_username")
                        private String twitterUsername;
                        @SerializedName("portfolio_url")
                        private String portfolioUrl;
                        @SerializedName("bio")
                        private String bio;
                        @SerializedName("location")
                        private String location;
                        @SerializedName("links")
                        private LinksBeanXXX links;
                        @SerializedName("profile_image")
                        private ProfileImageBeanX profileImage;
                        @SerializedName("instagram_username")
                        private String instagramUsername;
                        @SerializedName("total_collections")
                        private int totalCollections;
                        @SerializedName("total_likes")
                        private int totalLikes;
                        @SerializedName("total_photos")
                        private int totalPhotos;
                        @SerializedName("accepted_tos")
                        private boolean acceptedTos;

                        public String getId() {
                            return id;
                        }

                        public void setId(String id) {
                            this.id = id;
                        }

                        public String getUpdatedAt() {
                            return updatedAt;
                        }

                        public void setUpdatedAt(String updatedAt) {
                            this.updatedAt = updatedAt;
                        }

                        public String getUsername() {
                            return username;
                        }

                        public void setUsername(String username) {
                            this.username = username;
                        }

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public String getFirstName() {
                            return firstName;
                        }

                        public void setFirstName(String firstName) {
                            this.firstName = firstName;
                        }

                        public String getLastName() {
                            return lastName;
                        }

                        public void setLastName(String lastName) {
                            this.lastName = lastName;
                        }

                        public String getTwitterUsername() {
                            return twitterUsername;
                        }

                        public void setTwitterUsername(String twitterUsername) {
                            this.twitterUsername = twitterUsername;
                        }

                        public String getPortfolioUrl() {
                            return portfolioUrl;
                        }

                        public void setPortfolioUrl(String portfolioUrl) {
                            this.portfolioUrl = portfolioUrl;
                        }

                        public String getBio() {
                            return bio;
                        }

                        public void setBio(String bio) {
                            this.bio = bio;
                        }

                        public String getLocation() {
                            return location;
                        }

                        public void setLocation(String location) {
                            this.location = location;
                        }

                        public LinksBeanXXX getLinks() {
                            return links;
                        }

                        public void setLinks(LinksBeanXXX links) {
                            this.links = links;
                        }

                        public ProfileImageBeanX getProfileImage() {
                            return profileImage;
                        }

                        public void setProfileImage(ProfileImageBeanX profileImage) {
                            this.profileImage = profileImage;
                        }

                        public String getInstagramUsername() {
                            return instagramUsername;
                        }

                        public void setInstagramUsername(String instagramUsername) {
                            this.instagramUsername = instagramUsername;
                        }

                        public int getTotalCollections() {
                            return totalCollections;
                        }

                        public void setTotalCollections(int totalCollections) {
                            this.totalCollections = totalCollections;
                        }

                        public int getTotalLikes() {
                            return totalLikes;
                        }

                        public void setTotalLikes(int totalLikes) {
                            this.totalLikes = totalLikes;
                        }

                        public int getTotalPhotos() {
                            return totalPhotos;
                        }

                        public void setTotalPhotos(int totalPhotos) {
                            this.totalPhotos = totalPhotos;
                        }

                        public boolean isAcceptedTos() {
                            return acceptedTos;
                        }

                        public void setAcceptedTos(boolean acceptedTos) {
                            this.acceptedTos = acceptedTos;
                        }

                        public static class LinksBeanXXX {
                            @SerializedName("self")
                            private String self;
                            @SerializedName("html")
                            private String html;
                            @SerializedName("photos")
                            private String photos;
                            @SerializedName("likes")
                            private String likes;
                            @SerializedName("portfolio")
                            private String portfolio;
                            @SerializedName("following")
                            private String following;
                            @SerializedName("followers")
                            private String followers;

                            public String getSelf() {
                                return self;
                            }

                            public void setSelf(String self) {
                                this.self = self;
                            }

                            public String getHtml() {
                                return html;
                            }

                            public void setHtml(String html) {
                                this.html = html;
                            }

                            public String getPhotos() {
                                return photos;
                            }

                            public void setPhotos(String photos) {
                                this.photos = photos;
                            }

                            public String getLikes() {
                                return likes;
                            }

                            public void setLikes(String likes) {
                                this.likes = likes;
                            }

                            public String getPortfolio() {
                                return portfolio;
                            }

                            public void setPortfolio(String portfolio) {
                                this.portfolio = portfolio;
                            }

                            public String getFollowing() {
                                return following;
                            }

                            public void setFollowing(String following) {
                                this.following = following;
                            }

                            public String getFollowers() {
                                return followers;
                            }

                            public void setFollowers(String followers) {
                                this.followers = followers;
                            }
                        }

                        public static class ProfileImageBeanX {
                            @SerializedName("small")
                            private String small;
                            @SerializedName("medium")
                            private String medium;
                            @SerializedName("large")
                            private String large;

                            public String getSmall() {
                                return small;
                            }

                            public void setSmall(String small) {
                                this.small = small;
                            }

                            public String getMedium() {
                                return medium;
                            }

                            public void setMedium(String medium) {
                                this.medium = medium;
                            }

                            public String getLarge() {
                                return large;
                            }

                            public void setLarge(String large) {
                                this.large = large;
                            }
                        }
                    }
                }
            }
        }
    }
}

